# Arrays

Beginner level task for practicing arrays.


## Task Description

1. Implement 35 static methods in [CreatingArray.cs](WorkingWithArrays/CreatingArray.cs) file. See the method documentation and TODO comments.
1. Implement 31 static methods in [UsingIndexerForAccessingArrayElement.cs](WorkingWithArrays/UsingIndexerForAccessingArrayElement.cs) file. See the method documentation and TODO comments.
1. Implement 10 static methods in [UsingRanges.cs](WorkingWithArrays/UsingRanges.cs) file. See the method documentation and TODO comments.


## Fix Compiler Issues

Additional style and code checks are enabled for the projects in this solution to help you maintaining consistency of the project source code and avoiding silly mistakes. [Review the Error List](https://docs.microsoft.com/en-us/visualstudio/ide/find-and-fix-code-errors#review-the-error-list) in Visual Studio to see all compiler warnings and errors.

If a compiler error or warning message is not clear, [review errors details](https://docs.microsoft.com/en-us/visualstudio/ide/find-and-fix-code-errors#review-errors-in-detail) or google the error or warning code to get more information about the issue.


## Task Checklist

1. Rebuild the solution.
1. Fix all compiler warnings and errors.
1. Run all unit tests, make sure all unit tests completed successfully.
1. Review all changes, make sure the only code files (.cs) in Arrays project have changes. No changes in project files (.csproj) or in Arrays.Tests project.
1. Stage your changes, and create a commit.
1. Push your changes to remote repository.


## See also

* Tutorials
  * [Indices and ranges](https://docs.microsoft.com/en-us/dotnet/csharp/tutorials/ranges-indexes)
* C# Programming Guide
  * [Arrays](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/arrays/)
* .NET API
  * [Array Class](https://docs.microsoft.com/en-us/dotnet/api/system.array)
